﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace AddColumns
{
    public partial class Form1 : Form
    {
        DataSet ds = new DataSet();
        List<String> list = new List<string>();

        public Form1() => InitializeComponent();

        private void AddColumnsInDataTable(DataTable dt, List<string> ColumnstoAdd) =>
            ColumnstoAdd.ForEach(c => dt.Columns.Add(c.Split(':')[0],SettingType(c.Split(':')[1])));

        private Type SettingType(string v)
        {
            switch (v.ToLower().Replace(" ",""))
            {
                case "int":
                    return typeof(int);
                case "datetime":
                    return typeof(DateTime);
                case "decimal":
                    return typeof(decimal);
                case "string":
                    
                default:
                    return typeof(string);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e) =>
            dataGridView1.DataSource = ds.Tables[comboBox1.SelectedIndex];

        private void button1_Click(object sender, EventArgs e)
        {
            list = new List<string>(textBox1.Text.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries));
            if(list.Any() && ds.Tables.Count > 0)
            {
                AddColumnsInDataTable(ds.Tables[comboBox1.SelectedIndex], list);
                dataGridView1.DataSource = ds.Tables[comboBox1.SelectedIndex];
                textBox1.Text = "";
                MessageBox.Show($"{list.Count} columns have been added to {comboBox1.Items[comboBox1.SelectedIndex].ToString()} table sucessfully!");
            }
            else
            {
                if(!list.Any())
                    MessageBox.Show("Add some columns in the text box before trying to add them to the datatable");
                else
                    MessageBox.Show("Please select the dataset first before trying to add columns to nothing");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(ds.Tables.Count > 0)
            {
                saveFileDialog1.Filter = "Dat Files(*.dat)|*.dat";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    ds.WriteXml(saveFileDialog1.FileName, XmlWriteMode.WriteSchema);
                MessageBox.Show("Dataset is saved successfully!!");
            }
            else
            {
                MessageBox.Show("Can't save an empty dataset!!");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            dataGridView1.DataSource = null;
            openFileDialog1.Filter = "Dat Files (*.dat)|*.dat";
            if (openFileDialog1.ShowDialog() != DialogResult.OK) return;
            try
            {
                ds.ReadXml(openFileDialog1.FileName, XmlReadMode.ReadSchema);
                foreach (var table in ds.Tables)
                    comboBox1.Items.Add(table.ToString());

                comboBox1.SelectedIndex = 0;
                dataGridView1.DataSource = ds.Tables[comboBox1.SelectedIndex];
            }
            catch (Exception er)
            {
                comboBox1.Items.Clear();
                dataGridView1.DataSource = null;
                MessageBox.Show($"an error has occured: {er.Message}");
            }
        }
    }
}
